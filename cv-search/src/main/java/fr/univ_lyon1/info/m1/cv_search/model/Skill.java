package fr.univ_lyon1.info.m1.cv_search.model;


public class Skill {
    private String nameSkill;
    private int levelSkill;

    public Skill(String nom, int value) {
        this.nameSkill = nom;
        this.levelSkill = value;
    }

    public int getLevelSkill() {
        return levelSkill;
    }

    public String getNameSkill() {
        return nameSkill;
    }

    public void setSkill(String nameSkill, int levelSkill) {
        this.nameSkill = nameSkill;
        this.levelSkill = levelSkill;
    }
}
