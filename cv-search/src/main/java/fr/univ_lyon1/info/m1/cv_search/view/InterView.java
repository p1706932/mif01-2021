package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.ControllerApp;
import javafx.scene.Node;

import java.util.List;

public interface InterView {

    void setController(ControllerApp ca);

    Node createNewSkillWidget();

    Node createResultsWidget();

    Node createSearchWidget();

    Node createCurrentSearchSkillsWidget();

    void update();

    void showChosenApplicant(List<String> labelName);

    //Node createStrategyWidget();

}
