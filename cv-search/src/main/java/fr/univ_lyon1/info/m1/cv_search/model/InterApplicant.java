package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.Map;

public interface InterApplicant {

    int getSkill(String string);

    void setSkill(String string, int value);

    String getName();

    void setName(String name);

    Map<String, Integer> getListofSkill();

}
