package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.ControllerApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class PassiveView implements InterView {

    private ControllerApp controller;
    private HBox searchSkillsBox;
    private VBox resultBox;
    private ComboBox comboBox;
    private HBox selectStrategyBox;

    /**
     * Create the main view of the application.
     */
    public void createGui(Stage stage, int width, int height) {
        stage.setTitle("Search for CV");

        VBox root = new VBox();

        Node newSkillBox = createNewSkillWidget();
        root.getChildren().add(newSkillBox);

        //Node selectStrategyBox = createStrategyWidget();
        //root.getChildren().add(selectStrategyBox);

        Node searchSkillsBox = createCurrentSearchSkillsWidget();
        //root.getChildren().add(searchSkillsBox);

        Node search = createSearchWidget();
       // root.getChildren().add(search);

        Node resultBox = createResultsWidget();
        root.getChildren().add(resultBox);

        // Everything's ready: add it to the scene and display it
        Scene scene = new Scene(root, width, height);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void setController(ControllerApp controller) {
        this.controller = controller;
    }
    public ControllerApp getController() {
        return this.controller;
    }

    @Override
    public Node createNewSkillWidget() {
        HBox newSkillBox = new HBox();
        Label labelSkill = new Label("Résultat de vos recherche :");
        TextField textField = new TextField();
        Button submitButton = new Button("Add skill");
        //newSkillBox.getChildren().addAll(labelSkill, textField, submitButton);
        newSkillBox.getChildren().addAll(labelSkill);
        newSkillBox.setSpacing(20);

        EventHandler<ActionEvent> skillHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = textField.getText().strip();
                if (text.equals("")) {
                    return; // Do nothing
                }

                Button skillBtn = new Button(text);
                searchSkillsBox.getChildren().add(skillBtn);
                skillBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        searchSkillsBox.getChildren().remove(skillBtn);
                    }
                });

                textField.setText("");
                textField.requestFocus();
            }
        };
        submitButton.setOnAction(skillHandler);
        textField.setOnAction(skillHandler);
        return newSkillBox;
    }

    /**
     * Create the widget showing the list of applicants.
     */
    public Node createStrategyWidget() {

        HBox selectSrategyBox = new HBox();
        Label labelSrategy = new Label("Strategy:");

        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "All >= 50",
                        "All >= 60",
                        "Average >= 50"
                );
        comboBox = new ComboBox(options);
        comboBox.setValue("All => 50");
        selectSrategyBox.getChildren().addAll(labelSrategy, comboBox);
        //controller.setStrat((String) comboBox.getValue());
        return selectSrategyBox;
    }

    /**
     * Create the widget showing the list of applicants.
     */
    @Override
    public Node createResultsWidget() {
        resultBox = new VBox();
        return resultBox;
    }

    /**
     * Create the widget used to trigger the search.
     */
    @Override
    public Node createSearchWidget() {
        Button search = new Button("Search");
        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resultBox.getChildren().clear();
                List<Node> listSkill = searchSkillsBox.getChildren();
                controller.evaluate(listSkill, comboBox);
            }
        });
        return search;
    }

    /**
     * Create the widget showing the list of skills currently searched
     * for.
     */
    @Override
    public Node createCurrentSearchSkillsWidget() {
        searchSkillsBox = new HBox();
        return searchSkillsBox;
    }

    @Override
    public void update() {
        resultBox.getChildren().clear();
    }

    /**
     * show the applicants names after clicking search button.
     */
    public void showChosenApplicant(List<String> chosenList) {
        for (String name : chosenList) {
            resultBox.getChildren().add(new Label(name));
        }

    }
}
