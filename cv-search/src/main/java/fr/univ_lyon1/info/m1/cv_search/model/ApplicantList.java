package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ApplicantList implements Iterable<InterApplicant> {

    private List<InterApplicant> list = new ArrayList<>();

    public void add(InterApplicant a) {
        list.add(a);
    }

    public int size() {
        return list.size();
    }

    public String getName(int index) {
        return list.get(index).getName();
    }

    @Override
    public Iterator<InterApplicant> iterator() {
        return list.iterator();
    }

    /**
     * Clear the list of applicants.
     */
    public void clear() {
        list.clear();
    }

    /**
     * Sets the content of the applicant list.
     */
    public void setList(ApplicantList list) {
        this.list = list.list;
    }
}
