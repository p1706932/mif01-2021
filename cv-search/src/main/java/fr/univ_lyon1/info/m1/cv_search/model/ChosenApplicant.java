package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.Map;

public class ChosenApplicant implements InterApplicant {

    private InterApplicant chosen;

    public ChosenApplicant(InterApplicant applicant) {
        this.chosen = applicant;
    }

    public InterApplicant getChosen() {
        return chosen;
    }

    @Override
    public int getSkill(String string) {
        return this.chosen.getSkill(string);
    }

    @Override
    public void setSkill(String string, int value) {
        this.chosen.setSkill(string, value);
    }

    @Override
    public String getName() {
        return chosen.getName() + " avec une note de ";
    }

    @Override
    public void setName(String name) {
        chosen.setName(name);
    }

    @Override
    public Map<String, Integer> getListofSkill() {
        return null;
    }
}
