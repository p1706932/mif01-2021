package fr.univ_lyon1.info.m1.cv_search.evaluator;

import fr.univ_lyon1.info.m1.cv_search.model.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.ChosenApplicant;
import fr.univ_lyon1.info.m1.cv_search.model.InterApplicant;
import javafx.scene.Node;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;


public class AverageEvalute implements InterEvaluator {
    private List<String> labelName;
    private int sum;
    private int nbApplicant;

    @Override
    public List<String> evaluateApplicant(ApplicantList list, List<Node> listSkill) {
        labelName = new ArrayList<String>();
        sum = 0;
        nbApplicant = 0;

        if (listSkill.isEmpty()) {
            labelName.add("Filtrage des compétences avec une moyenne >= 50 :"
                    + " Veuillez Entrer une compétence !");
            return labelName;
        }

        for (InterApplicant a : list) {
            sum = 0;
            nbApplicant = 0;
            for (Node skill : listSkill) {
                String skillName = ((Button) skill).getText();
                sum += a.getSkill(skillName);
                ++nbApplicant;
            }

            InterApplicant chosenOne = new ChosenApplicant(a);
            if (nbApplicant != 0 && sum / nbApplicant >= 50) {
                labelName.add(chosenOne.getName() + " " + (sum / nbApplicant));
            }
        }

        return labelName;
    }
}
