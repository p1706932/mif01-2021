package fr.univ_lyon1.info.m1.cv_search.view;

import fr.univ_lyon1.info.m1.cv_search.controller.ControllerApp;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;

public class Views implements InterView {
    private List<InterView> views;

    public Views() {
        views = new ArrayList<InterView>();
    }

    public void addView(InterView view) {
        views.add(view);
    }


    @Override
    public void setController(ControllerApp ca) {
        for (InterView view : this.views) {
            view.setController(ca);
        }
    }

    @Override
    public Node createNewSkillWidget() {
        for (InterView view : this.views) {
            view.createNewSkillWidget();
        }
        return null;
    }

    @Override
    public Node createResultsWidget() {
        for (InterView view : this.views) {
            view.createResultsWidget();
        }
        return null;
    }

    @Override
    public Node createSearchWidget() {
        for (InterView view : this.views) {
            view.createSearchWidget();
        }
        return null;
    }

    @Override
    public Node createCurrentSearchSkillsWidget() {
        for (InterView view : this.views) {
            view.createCurrentSearchSkillsWidget();
        }
        return null;
    }

    @Override
    public void update() {
        for (InterView view : this.views) {
            view.update();
        }
    }

    @Override
    public void showChosenApplicant(List<String> labelName) {
        for (InterView view : this.views) {
            view.showChosenApplicant(labelName);
        }
    }
}
