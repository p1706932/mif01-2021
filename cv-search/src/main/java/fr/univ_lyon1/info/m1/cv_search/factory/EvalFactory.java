package fr.univ_lyon1.info.m1.cv_search.factory;

import fr.univ_lyon1.info.m1.cv_search.evaluator.InterEvaluator;
import fr.univ_lyon1.info.m1.cv_search.evaluator.CumulEvaluator;
import fr.univ_lyon1.info.m1.cv_search.evaluator.AverageEvalute;
import fr.univ_lyon1.info.m1.cv_search.evaluator.SupTo60Evaluate;
import fr.univ_lyon1.info.m1.cv_search.evaluator.SupTo50Evaluate;

public final class EvalFactory {
    public enum EvalType {
        sup60,
        sup50,
        avg50,
        cumul
    };

    private EvalFactory() {

    }

    public static InterEvaluator makeEval(EvalType type) {
        switch (type) {
            case cumul: return new CumulEvaluator();
            case sup60: return new SupTo60Evaluate();
            case avg50: return new AverageEvalute();
            case sup50:
            default: return new SupTo50Evaluate();
        }
    }
}
