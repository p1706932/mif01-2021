package fr.univ_lyon1.info.m1.cv_search.controller;

import java.io.File;

import fr.univ_lyon1.info.m1.cv_search.evaluator.InterEvaluator;
import fr.univ_lyon1.info.m1.cv_search.factory.EvalFactory;
import fr.univ_lyon1.info.m1.cv_search.model.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.ApplicantListBuilder;
import fr.univ_lyon1.info.m1.cv_search.view.InterView;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;

import java.util.List;

public class ControllerApp {

    private InterView view;
    private ApplicantList listApplicants;
    private List<String> labelName;

    private InterEvaluator evaluateSupTo60;
    private InterEvaluator evaluateSupTo50;
    private InterEvaluator evaluateAvg50;
    private InterEvaluator evaluateCumul;


    /**
     * constructeur .
     */
    public ControllerApp(InterView view) {
        super();
        this.view = view;
        listApplicants = new ApplicantListBuilder(new File(".")).build();
        view.setController(this);
    }

    /**
     * evaluate the wanted list of applicant.
     */
    public void evaluate(List<Node> listSkill, ComboBox comboBox) {

        listApplicants = new ApplicantListBuilder(new File(".")).build();

        evaluateSupTo60 = EvalFactory.makeEval(EvalFactory.EvalType.sup60);
        evaluateSupTo50 = EvalFactory.makeEval(EvalFactory.EvalType.sup50);
        evaluateAvg50 = EvalFactory.makeEval(EvalFactory.EvalType.avg50);
        evaluateCumul = EvalFactory.makeEval(EvalFactory.EvalType.cumul);

        if (comboBox.getValue() == null) {
            labelName.add("Choisissez une strategie");
        }

        switch ((String) comboBox.getValue()) {

            case "Cumul points":
                view.update();
                labelName = evaluateCumul.evaluateApplicant(listApplicants, listSkill);
                break;

            case "All >= 60":
                view.update();
                labelName = evaluateSupTo60.evaluateApplicant(listApplicants, listSkill);
                break;

            case "Average >= 50":
                view.update();
               labelName = evaluateAvg50.evaluateApplicant(listApplicants, listSkill);
               break;

            case "All => 50":

            default:
                view.update();
                labelName = evaluateSupTo50.evaluateApplicant(listApplicants, listSkill);
        }
        view.showChosenApplicant(labelName);

    }

    public ApplicantList getListApplicants() {
        return this.listApplicants;
    }

    public List<String> getLabelName() {
        return this.labelName;
    }

}
