package fr.univ_lyon1.info.m1.cv_search.evaluator;

import fr.univ_lyon1.info.m1.cv_search.model.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.ChosenApplicant;
import fr.univ_lyon1.info.m1.cv_search.model.InterApplicant;
import javafx.scene.Node;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;

public class SupTo60Evaluate implements InterEvaluator {
    private List<String> labelName;
    private int sum;
    private int nbApplicant;


    @Override
    public List<String> evaluateApplicant(ApplicantList list, List<Node> listSkill) {
        labelName = new ArrayList<String>();
        sum = 0;
        nbApplicant = 0;

        boolean selected = true;

        if (listSkill.isEmpty()) {
            labelName.add("Filtrage des compétences superieures ou égales à 60 : "
                    + "Veuillez entrer une compétence !");
            return labelName;
        }


        for (InterApplicant a : list) {
            sum = 0;
            nbApplicant = 0;
            for (Node skill : listSkill) {
                String skillName = ((Button) skill).getText();
                sum += a.getSkill(skillName);
                ++nbApplicant;

            }
            if ((sum / nbApplicant) < 60) {
                selected = false;
                break;
            } else {

                InterApplicant chosenOne = new ChosenApplicant(a);
                if (selected && nbApplicant != 0) {
                    labelName.add(chosenOne.getName() + " " + (sum / nbApplicant));
                } else if (selected) {
                    labelName.add(chosenOne.getName());
                }
            }
        }

        return labelName;
    }
}
