package fr.univ_lyon1.info.m1.cv_search.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Applicant implements InterApplicant {
    private Map<String, Integer> skills = new HashMap<>();
    private ArrayList<Skill> listOfSkill = new ArrayList<>(); // Rajout
    private String name;


    public int getSkill(String string) {
        return skills.getOrDefault(string, 0);
    }

    public void setSkill(String string, int value) {
        skills.put(string, value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Rajout
    public void addToList(String string, int value) {
        Skill aSkill = new Skill(string, value);
        listOfSkill.add(aSkill);
    }

    public Map<String, Integer> getListofSkill() {
        return skills;
    }

}
