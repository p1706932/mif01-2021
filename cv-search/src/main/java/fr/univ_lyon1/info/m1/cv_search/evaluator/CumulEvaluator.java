package fr.univ_lyon1.info.m1.cv_search.evaluator;

import fr.univ_lyon1.info.m1.cv_search.model.ApplicantList;
import fr.univ_lyon1.info.m1.cv_search.model.InterApplicant;
import javafx.scene.Node;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Comparator;
import java.util.LinkedHashMap;


public class CumulEvaluator implements InterEvaluator {

    private List<String> labelName;
    private int sum;
    private int nbApplicant;
    private Map<InterApplicant, Integer> mapAtrier = new HashMap<>();

    @Override
    public List<String> evaluateApplicant(ApplicantList list, List<Node> listSkill) {
        labelName = new ArrayList<>();
        sum = 0;
        nbApplicant = 0;

        labelName.add("Les candidats ci-dessous sont triées par ordre croissant "
                + "en fonction de leur points (totaux) : \n");
        labelName.add("\n");

        for (InterApplicant a : list) {

            for (Map.Entry<String, Integer> map : a.getListofSkill().entrySet()) {
                sum += map.getValue();

            }

            mapAtrier.put(a, sum);
            //trier mapAtrier


            String somme = String.valueOf(sum);
           // labelName.add(a.getName() + " avec un score total de " + somme);
            sum = 0;
        }
        Map<InterApplicant, Integer> map = sort(mapAtrier);
        for (Map.Entry<InterApplicant, Integer> entry : map.entrySet()) {
            labelName.add(entry.getKey().getName() + " avec un score total de "
                    + entry.getValue().toString() + "\n");
        }

        return labelName;
    }

    private static Map sort(Map<InterApplicant, Integer> map) {
        List linkedlist = new LinkedList(map.entrySet());
        Collections.sort(linkedlist, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        Map sortedHashMap = new LinkedHashMap();
        for (Iterator it = linkedlist.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
