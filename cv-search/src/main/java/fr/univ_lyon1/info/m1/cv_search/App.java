package fr.univ_lyon1.info.m1.cv_search;

import fr.univ_lyon1.info.m1.cv_search.controller.ControllerApp;
import fr.univ_lyon1.info.m1.cv_search.view.PassiveView;
import fr.univ_lyon1.info.m1.cv_search.view.View;
import fr.univ_lyon1.info.m1.cv_search.view.Views;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class for the application (structure imposed by JavaFX).
 */
public class App extends Application {

    /**
     * With javafx, start() is called when the application is launched.
     */
    @Override
    public void start(Stage stage) throws Exception {

        Views views = new Views(); //liste de vue

        View view = new View();
        view.createGui(stage, 600, 600);
        views.addView(view);

        PassiveView passiveView = new PassiveView();
        passiveView.createGui(new Stage(), 500, 500);
        views.addView(passiveView);

        ControllerApp ca = new ControllerApp(views);
    }


    /**
     * A main method in case the user launches the application using
     * App as the main class.
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}
