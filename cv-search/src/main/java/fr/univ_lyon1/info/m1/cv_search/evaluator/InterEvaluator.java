package fr.univ_lyon1.info.m1.cv_search.evaluator;

import fr.univ_lyon1.info.m1.cv_search.model.ApplicantList;
import javafx.scene.Node;

import java.util.List;

public interface InterEvaluator {
    List<String> evaluateApplicant(ApplicantList list, List<Node> listSkill);
}
