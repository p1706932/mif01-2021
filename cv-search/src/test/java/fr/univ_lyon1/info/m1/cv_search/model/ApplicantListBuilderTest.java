package fr.univ_lyon1.info.m1.cv_search.model;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class ApplicantListBuilderTest {
    @Test
    public void testCreationList() {
        // Given
        ApplicantListBuilder builder = new ApplicantListBuilder(new File("."));
        String name = "John Smith";

        // When
        ApplicantList list = builder.build();


        // Then
        assertThat(true, is(list.size() > 0));
    }
}