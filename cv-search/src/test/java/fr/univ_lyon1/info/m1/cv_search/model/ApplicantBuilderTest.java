package fr.univ_lyon1.info.m1.cv_search.model;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class ApplicantBuilderTest {

    private final String filename = "applicant5.yaml";
    @Test
    public void testFile() {
        assertThat(true, is(filename.endsWith(".yaml")));
    }

    @Test
    public void testStructure() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder(filename);

        // When
        InterApplicant a = builder.build();

        // Then
        assertThat(true, is(builder.getFile().toString().endsWith(".yaml")));
        assertThat(true, is(a.getName() != null));
        assertThat("Younes", is(a.getName()));
        assertThat(70, is(a.getSkill("html")));
    }
}