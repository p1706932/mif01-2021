package fr.univ_lyon1.info.m1.cv_search.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class ApplicantListTest {
    @Test
    public void testlist() {
        // Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant1.yaml");

        // When
        ApplicantList list = new ApplicantList();
        list.add(builder.build());

        // Then
        assertThat(true, is(list.size() > 0));
        assertThat("John Smith", is(list.getName(0)));

    }
}