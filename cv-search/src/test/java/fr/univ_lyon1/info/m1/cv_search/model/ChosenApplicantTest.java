package fr.univ_lyon1.info.m1.cv_search.model;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

public class ChosenApplicantTest {
    @Test
    public void testReadChosenApplicant() {
        //Given
        ApplicantBuilder builder = new ApplicantBuilder("applicant2.yaml");

        //When
        InterApplicant a = new ChosenApplicant(builder.build());

        //Then
        assertThat(40, is(a.getSkill("c")));
        assertThat("Foo Bar avec une note de ", is(a.getName()));
    }

    @Test
    public void testReadManyChosenApplicant() {
        //Given
        ApplicantListBuilder builder = new ApplicantListBuilder(new File("."));

        //When
        ApplicantList list =  builder.build();

        //Then
        boolean julieFound = false;
        for (InterApplicant a : list) {
            ChosenApplicant found = new ChosenApplicant(a);
            if (found.getName().equals("Julie avec une note de ")) {
                assertThat(50, is(found.getSkill("java")));
                assertThat(95, is(found.getSkill("python")));
                julieFound = true;
            }
        }
        assertThat(julieFound, is(true));
    }
}